#include<iostream>

using namespace std;

struct Student {
	char* nume;
	char* prenume;
	int id;
};

struct NodABC {
	NodABC* dr;
	NodABC* st;
	Student* stud;
};

Student* creareStudent(char* nume, char*prenume, int id)
{
	Student* stud = new Student;
	stud->id = id;

	stud->nume = new char[strlen(nume) + 1];
	strcpy(stud->nume, nume);

	stud->prenume = new char[strlen(prenume) + 1];
	strcpy(stud->prenume, prenume);

	return stud;
}

NodABC* inserareNodABC(NodABC* abc, Student* stud)
{
	if (abc)
	{
		if (abc->stud->id > stud->id)
			abc->st = inserareNodABC(abc->st, stud);
		else
			if (abc->stud->id < stud->id)
				abc->dr = inserareNodABC(abc->dr, stud);
			else
				cout << "Nodul deja este inserat!" << endl;
		return abc;
	}
	else
	{
		NodABC* ab = new NodABC;
		ab->st = NULL;
		ab->dr = NULL;
		ab->stud = stud;
		
		return ab;
	}
}

//NodABC* inserareNodABC(NodABC* abc, Student* stud)
//{
//	NodABC* nou = new NodABC;
//	nou->stud = stud;
//	nou->st = NULL;
//	nou->dr = NULL;
//
//	if (abc == NULL)
//		return nou;
//	else
//	{
//		if (abc->stud->id < stud->id)
//		{
//			if (abc->dr != NULL)
//				inserareNodABC(abc->dr, stud);
//			else
//				abc->dr = nou;
//		}
//		else
//			if (abc->stud->id > stud->id)
//			{
//				if (abc->st != NULL)
//					inserareNodABC(abc->st, stud);
//				else
//					abc->st = nou;
//			}
//			else
//				return abc;
//	}
//	return abc;
//}

void afisareStudent(Student* stud)
{
	cout << stud->id << " " << stud->nume << " " << stud->prenume << endl;
}

void SRD(NodABC* abc)
{
	if (abc)
	{
		SRD(abc->st);
		afisareStudent(abc->stud);
		SRD(abc->dr);
	}
}

void SDR(NodABC* abc)
{
	if (abc)
	{
		SRD(abc->st);
		SRD(abc->dr);
		afisareStudent(abc->stud);
	}
}

NodABC* stergereNodABC(NodABC* rad, Student* stud)
{
	if (rad)
	{
		if (rad->stud->id < stud->id)
			rad->dr = stergereNodABC(rad->dr, stud);
		else
		{
			if (rad->stud->id > stud->id)
				rad->st = stergereNodABC(rad->st, stud);
			else
			{
				if (rad->st == rad->dr)
				{
					delete rad->stud;
					rad = NULL;
				}
				else
				{
					if (rad->st == NULL)
					{
						NodABC* temp = rad->dr;
						delete rad->stud;
						delete rad;
						rad = temp;
					}
					else
					{
						if (rad->dr == NULL)
						{
							NodABC* temp = rad->st;
							delete rad->stud;
							delete rad;
							rad = temp;
						}
						else
						{


						}
					}
				}
			}
		}		
	}
	return rad;
}

Student* StudDr(NodABC* abc)
{
	NodABC* temp = abc;
	NodABC* aux;

	while (temp->dr->dr)
	{
		temp = temp->dr;
	}

	Student* stud = temp->dr->stud;
	temp->dr = NULL;

	return stud;
}

void main()
{
	Student* s1 = creareStudent("AAAAA", "aaaa", 1);
	Student* s2 = creareStudent("BBBBB", "bbbbb", 2);
	Student* s3 = creareStudent("CCCCC", "cccc", 3);
	Student* s4 = creareStudent("DDDDD", "dddd", 4);
	Student* s5 = creareStudent("EEEEE", "eeee", 5);
	Student* s6 = creareStudent("FFFFF", "ffff", 6);
	Student* s7 = creareStudent("GGGGG", "gggg", 7);
	Student* s8 = creareStudent("HHHHH", "hhhh", 8);
	Student* s9 = creareStudent("IIIII", "iiii", 9);
	Student* s10 = creareStudent("JJJJJ", "jjjj", 10);
	Student* s11 = creareStudent("KKKKK", "kkkk", 11);

	NodABC* abc = NULL;
	
	abc = inserareNodABC(abc, s6);
	abc = inserareNodABC(abc, s9);
	abc = inserareNodABC(abc, s5);
	abc = inserareNodABC(abc, s11);
	abc = inserareNodABC(abc, s1);
	abc = inserareNodABC(abc, s2);
	abc = inserareNodABC(abc, s7);
	abc = inserareNodABC(abc, s4);
	abc = inserareNodABC(abc, s8);
	abc = inserareNodABC(abc, s10);
	abc = inserareNodABC(abc, s3);

	SRD(abc);
	cout << endl << endl << endl;
	SDR(abc);

	cout << endl << endl << endl;
	Student* stud = StudDr(abc);
	afisareStudent(stud);
	cout << endl << endl << endl;
	SRD(abc);
}