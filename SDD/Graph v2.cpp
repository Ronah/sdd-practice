//#include<iostream>
//
//using namespace std;
//
//struct Student {
//	int id;
//	char* nume;
//	char* prenume;
//};
//
//struct NodSecundar;
//
//struct NodPrincipal {
//	NodPrincipal* next;
//	Student* stud;
//	NodSecundar* vecini;
//};
//
//struct NodSecundar {
//	NodSecundar* next;
//	NodPrincipal* np;
//};
//
//struct NodStiva {
//	NodStiva* next;
//	NodPrincipal* np;
//};
//
//NodStiva* inserareStiva(NodStiva* nv, NodPrincipal*np)
//{
//	NodStiva* nou = new NodStiva;
//	nou->next = nv;
//	nou->np = np;
//
//	return nou;
//}
//
//NodStiva* extragereStiva(NodStiva* nv, NodPrincipal*& np)
//{
//	np = nv->np;
//	return nv->next;
//}
//
//NodPrincipal* inserareSfarsitPricipal(NodPrincipal* np, Student* stud)
//{
//
//	NodPrincipal* nou = new NodPrincipal;
//	nou->next = NULL;
//	nou->vecini = NULL;
//	nou->stud = stud;
//
//	if (np == NULL)
//	{
//		return nou;
//	}
//	else
//	{
//		NodPrincipal* temp = np;
//		while (temp->next)
//		{
//			temp = temp->next;
//		}
//		temp->next = nou;
//
//		return np;	
//	}
//}
//
//NodSecundar* inserareInceput(NodSecundar* ns, NodPrincipal* np)
//{
//	NodSecundar* nou = new NodSecundar;
//	nou->next = ns;
//	nou->np = np;
//
//	return nou;
//}
//
//void inserareElement(NodPrincipal*np, Student* stud, Student* s)
//{
//	NodPrincipal* temp = np;
//	while (temp->stud->id != stud->id)
//	{
//		temp = temp->next;
//	}
//
//	NodPrincipal* toInsert = np;
//	while (toInsert->stud->id != s->id)
//	{
//		toInsert = toInsert->next;
//	}
//
//	temp->vecini = inserareInceput(temp->vecini, toInsert);
//}
//
//Student* creareStudent(int id, char* nume, char*prenume)
//{
//	Student* nou = new Student;
//	nou->id = id;
//
//	nou->nume = new char[strlen(nume) + 1];
//	strcpy(nou->nume, nume);
//
//	nou->prenume = new char[strlen(prenume) + 1];
//	strcpy(nou->prenume, prenume);
//	
//	return nou;
//}
//
//void afisareStudent(Student* stud)
//{
//	cout << stud->id << " " << stud->nume << " " << stud->prenume << endl;
//}
//
//void afisarePrincipal(NodPrincipal* np)
//{
//	NodPrincipal* temp = np;
//	while (temp)
//	{
//		afisareStudent(temp->stud);
//		temp = temp->next;
//	}
//}
//
//void afisareVecini(NodSecundar* ns)
//{
//	NodSecundar* temp = ns;
//	while (temp)
//	{
//		afisareStudent(temp->np->stud);
//		temp = temp->next;
//	}
//}
//
//void afisareGraf(NodPrincipal* np)
//{
//	NodPrincipal* temp = np;
//	while (temp)
//	{
//		afisareStudent(temp->stud);
//		afisareVecini(temp->vecini);
//		cout << endl;
//		temp = temp->next;
//	}
//}
//
//bool cautareVizitat(NodStiva* ns, NodPrincipal* np)
//{
//	NodStiva* temp = ns;
//	bool status = false;
//	while (temp)
//	{
//		if (temp->np->stud->id == np->stud->id)
//			status = true;
//		temp = temp->next;
//	}
//
//	return status;
//}
//
//NodStiva* DF(NodPrincipal* np)
//{
//	NodStiva* vecini = NULL;
//	NodStiva* drum = NULL;
//	NodStiva* vizitat = NULL;
//
//	vecini = inserareStiva(vecini, np);
//	vizitat = inserareStiva(vizitat, np);
//
//	while (vecini)
//	{
//		NodPrincipal* extras = NULL;
//
//		vecini = extragereStiva(vecini, extras);
//		while (cautareVizitat(vecini, extras))
//			vecini = extragereStiva(vecini, extras);
//		drum = inserareStiva(drum, extras);
//
//		NodSecundar* vec = extras->vecini;
//		vizitat = inserareStiva(vizitat, extras);
//
//		while (vec)
//		{
//			if (vec->np)
//			{
//				if (!cautareVizitat(vizitat, vec->np))
//				{
//
//					vecini = inserareStiva(vecini, vec->np);
//					//vizitat = inserareStiva(vizitat, vec->np);
//				}
//			}
//			vec = vec->next;
//		}
//	}
//	return drum;
//}
//
//void afisareStiva(NodStiva* ns)
//{
//	NodStiva* temp = ns;
//	while (temp)
//	{
//		afisareStudent(temp->np->stud);
//		temp = temp->next;
//	}
//}
//
//void main()
//{
//	Student* s1 = creareStudent(1, "AAAA", "aaaa");
//	Student* s2 = creareStudent(2, "BBBB", "bbbb");
//	Student* s3 = creareStudent(3, "CCCC", "cccc");
//	Student* s4 = creareStudent(4, "DDDD", "dddd");
//	Student* s5 = creareStudent(5, "EEEE", "eeee");
//	Student* s6 = creareStudent(6, "FFFF", "ffff");
//	Student* s7 = creareStudent(7, "GGGG", "gggg");
//	Student* s8 = creareStudent(8, "HHHH", "hhhh");
//
//	NodPrincipal* np = NULL;
//	np = inserareSfarsitPricipal(np, s1);
//	np = inserareSfarsitPricipal(np, s2);
//	np = inserareSfarsitPricipal(np, s3);
//	np = inserareSfarsitPricipal(np, s4);
//	np = inserareSfarsitPricipal(np, s5);
//	np = inserareSfarsitPricipal(np, s6);
//	np = inserareSfarsitPricipal(np, s7);
//	np = inserareSfarsitPricipal(np, s8);
//
//	afisarePrincipal(np);
//
//	inserareElement(np, s1, s2);
//	inserareElement(np, s1, s3);
//
//	inserareElement(np, s2, s1);
//	inserareElement(np, s2, s5);
//
//	inserareElement(np, s3, s1);
//	inserareElement(np, s3, s5);
//
//	inserareElement(np, s4, s5);
//	inserareElement(np, s4, s6);
//	inserareElement(np, s4, s8);
//
//	inserareElement(np, s5, s2);
//	inserareElement(np, s5, s3);
//	inserareElement(np, s5, s4);
//	inserareElement(np, s5, s7);
//
//	inserareElement(np, s6, s4);
//	inserareElement(np, s6, s7);
//	inserareElement(np, s6, s8);
//
//	inserareElement(np, s7, s5);
//	inserareElement(np, s7, s6);
//	inserareElement(np, s7, s8);
//
//	inserareElement(np, s8, s6);
//	inserareElement(np, s8, s7);
//	inserareElement(np, s8, s4);
//
//	cout << endl << endl << endl;
//
//	afisareGraf(np);
//
//	cout << endl << endl << endl;
//	NodStiva* ns = NULL;
//	ns = DF(np);
//	afisareStiva(ns);
//}