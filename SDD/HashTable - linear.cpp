//#include<iostream>
//
//using namespace std;
//
//struct Student {
//	char* nume;
//	char* prenume;
//	int id;
//};
//
//struct HashTable {
//	int dim;
//	Student** stud;
//};
//
//Student* creareStudent(char* nume, char* prenume, int id)
//{
//	Student* stud = new Student;
//	stud->id = id;
//
//	stud->nume = new char[strlen(nume) + 1];
//	strcpy(stud->nume, nume);
//
//	stud->prenume = new char[strlen(prenume) + 1];
//	strcpy(stud->prenume, prenume);
//	
//	return stud;
//}
//
//int functieHash(Student* s, int dim)
//{
//	int suma = 0;
//	for (int i = 0; i < strlen(s->nume); i++)
//		suma += s->nume[i];
//	return suma%dim;
//}
//
//HashTable* initHashTable(int dim)
//{
//	HashTable* nou = new HashTable;
//	nou->dim = dim;
//	nou->stud = new Student*[dim];
//	for (int i = 0; i < dim; i++)
//	{
//		nou->stud[i] = NULL;
//	}
//
//	return nou;
//}
//
//void inserareHashTable(HashTable* ht, Student* stud)
//{
//	int poz = functieHash(stud, ht->dim);
//	while (ht->stud[poz])
//		poz++;
//	ht->stud[poz] = stud;
//}
//
//void afisareStudent(Student* stud)
//{
//	cout << stud->id << " " << stud->nume << " " << stud->prenume << endl;
//}
//
//void afisareHashTable(HashTable* ht)
//{
//	for (int i = 0; i < ht->dim; i++)
//	{
//		if (ht->stud[i])
//			afisareStudent(ht->stud[i]);
//	}
//}
//
//void main()
//{
//	HashTable* ht = initHashTable(100);
//
//	Student* s1 = creareStudent("AAAA", "aaaa", 1);
//	Student* s2 = creareStudent("AAAA", "aaaa", 2);
//	Student* s3 = creareStudent("BBBBBB", "bbb", 3);
//	Student* s4 = creareStudent("CCC", "cccccc", 4);
//	Student* s5 = creareStudent("DDDD", "dd", 2);
//
//	inserareHashTable(ht, s1);
//	inserareHashTable(ht, s2);
//	inserareHashTable(ht, s3);
//	inserareHashTable(ht, s4);
//	inserareHashTable(ht, s5);
//
//	afisareHashTable(ht);
//}