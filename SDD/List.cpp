//#include<iostream>
//
//using namespace std;
//
//struct Student{
//	char* nume;
//	char* prenume;
//	int id;
//	char facultate[30];
//};
//
//struct Nod {
//	Student* stud;
//	Nod* next;
//};
//
//Nod* inserareInceput(Nod* lista, Student* stud)
//{
//	Nod* nou = new Nod;
//	nou->next = lista;
//	nou->stud = stud;
//
//	return nou;
//}
//
//void inserareSfarsit(Nod* lista, Student* stud)
//{
//	Nod* temp = lista;
//	while (temp->next)
//	{
//		temp = temp->next;
//	}
//	Nod* nou = new Nod;
//	temp->next = nou;
//	nou->next = NULL;
//	nou->stud = stud;
//}
//
//Student* creareStudent(char* nume, char* prenume, char* facultate, int id)
//{
//	Student* stud = new Student;
//	stud->id = id;
//	strcpy(stud->facultate, facultate);
//	stud->nume = new char[strlen(nume) + 1];
//	strcpy(stud->nume, nume);
//	stud->prenume = new char[strlen(prenume) + 1];
//	strcpy(stud->prenume, prenume);
//
//	return stud;
//}
//
//Student* cautareStudent(Nod* list, int id)
//{
//	Nod* temp = list;
//	Student* stud = NULL;
//	while (temp)
//	{
//		if (temp->stud->id == id)
//			return stud = temp->stud;
//		else
//			temp = temp->next;
//	}
//	return stud;
//}
//
//void afisareStudent(Student* s)
//{
//	cout << s->id << " " << s->facultate << " " << s->nume << " " << s->prenume << endl;
//}
//
//void afisareLista(Nod* list)
//{
//	Nod* temp;
//	temp = list;
//	while (temp)
//	{
//		afisareStudent(temp->stud);
//		temp = temp->next;
//	}
//}
//
//void dropNod(Nod* toDelete)
//{
//	delete[] toDelete->stud->nume;
//	delete[] toDelete->stud->prenume;
//	delete toDelete->stud;
//	delete toDelete;
//}
//
//Nod* inserarePozitieData(Nod* list, Student* stud, int poz)
//{
//	if (poz == 0)
//		return inserareInceput(list, stud);
//	else
//	{
//		Nod* nou = new Nod;
//		nou->stud = stud;
//
//		Nod* temp = list;
//		int i = 1;
//		while (temp)
//		{
//			if (i != poz)
//			{
//				temp = temp->next;
//				i++;
//			}
//			else
//			{
//				nou->next = temp->next;
//				temp->next = nou;
//				i++;
//				return list;
//			}
//		}
//	}
//}
//
//void main()
//{
//	Student* s1 = creareStudent("AAAA", "aaaaaa", "CSIE", 1);
//	Student* s2 = creareStudent("BBBB", "bbbbb", "FABIZ", 2);
//	Student* s3 = creareStudent("CCCCCCC", "ccc", "CSIE", 3);
//	Student* s4 = creareStudent("DDDDD", "dddddd", "CIG", 4);
//	Student* s5 = creareStudent("EEEEEE", "eeeee", "COM", 5);
//
//	Nod* list = new Nod;
//	list->next = NULL; 
//	list->stud = s1;
//	list = inserareInceput(list, s2);
//	list = inserareInceput(list, s3);
//	list = inserareInceput(list, s4);
//	list = inserareInceput(list, s5);
//
//	cout << "Inserare inceput: " << endl;
//	afisareLista(list);
//	cout << endl << endl << endl;
//
//	Nod* lista2 = new Nod;
//	lista2->next = NULL;
//	lista2->stud = s1;
//
//	cout << "Inserare sfarsit: " << endl;
//	inserareSfarsit(lista2, s2);
//	inserareSfarsit(lista2, s3);
//	inserareSfarsit(lista2, s4);
//	inserareSfarsit(lista2, s5);
//	afisareLista(lista2);
//	cout << endl << endl << endl;
//
//	cout << "Cautare student: " << endl;
//	Student* search = cautareStudent(lista2, 7);
//	if (search != NULL)
//		afisareStudent(search);
//	else
//		cout << "Studentul nu a fost gasit: " << endl;
//
//	Nod* temp = new Nod;
//	temp->stud = creareStudent("ADWA", "Dwadwadsa", "dws", 43);
//	dropNod(temp);
//
//	cout << endl << endl << endl;
//	cout << "Inserare pozitie data: " << endl;
//	Student* s10 = creareStudent("GGGG", "ggg", "CSIE", 23);
//	lista2 = inserarePozitieData(lista2, s10, 5);
//	afisareLista(lista2);
//}