//#include<iostream>
//
//using namespace std;
//
//struct Student {
//	char* nume;
//	char* prenume;
//	int id;
//};
//
//struct Nod {
//	Student * stud;
//	Nod* next;
//};
//
//struct HashTable{
//	int dim;
//	Nod** ht;
//};
//
//Student * creareStudent(char* nume, char* prenume, int id)
//{
//	Student* stud = new Student;
//	stud->id = id;
//
//	stud->nume = new char[strlen(nume) + 1];
//	strcpy(stud->nume, nume);
//
//	stud->prenume = new char[strlen(prenume) + 1];
//	strcpy(stud->prenume, prenume);
//
//	return stud;
//}
//
//HashTable* initializareHashTable(int dim)
//{
//	HashTable* nou = new HashTable;
//	nou->dim = dim;
//	nou->ht = new Nod*[dim];
//	for (int i = 0; i < dim; i++)
//		nou->ht[i] = NULL;
//	
//	return nou;
//}
//
//int calculareHash(Student* stud, int dim)
//{
//	int suma = 0;
//	for (int i = 0; i < strlen(stud->nume); i++)
//	{
//		suma += stud->nume[i];
//	}
//
//	return suma % dim;
//}
//
//Nod* inserareInceputLista(Nod* lst, Student* stud)
//{
//	Nod* nou = new Nod;
//	nou->next = lst;
//	nou->stud = stud;
//
//	return nou;
//}
//
//void inserareHashTable(HashTable* hasht, Student* stud)
//{
//	int poz = calculareHash(stud, hasht->dim);
//	if (!hasht->ht[poz])
//	{
//		Nod* nou = new Nod;
//		nou->next = NULL;
//		nou->stud = stud;
//		hasht->ht[poz] = nou;
//	}
//	else
//	{
//		hasht->ht[poz] = inserareInceputLista(hasht->ht[poz], stud);
//	}
//}
//
//void afisareLista(Nod* list)
//{
//	Nod* temp = list;
//	while (temp)
//	{
//		cout << temp->stud->id << " " << temp->stud->nume << " " << temp->stud->prenume << endl;
//		temp = temp->next;
//	}
//}
//
//void afisareHashTable(HashTable* hasht)
//{
//	for (int i = 0; i < hasht->dim; i++)
//	{
//		if (hasht->ht[i])
//			afisareLista(hasht->ht[i]);
//	}
//}
//
//void main()
//{
//	HashTable* ht = initializareHashTable(50);
//
//	Student* s1 = creareStudent("AAAA", "aaaa", 1);
//	Student* s2 = creareStudent("AAAA", "aaaa", 2);
//	Student* s3 = creareStudent("BBBBBB", "bbb", 3);
//	Student* s4 = creareStudent("CCC", "cccccc", 4);
//	Student* s5 = creareStudent("DDDD", "dd", 2);
//
//	inserareHashTable(ht, s1);
//	inserareHashTable(ht, s2);
//	inserareHashTable(ht, s3);
//	inserareHashTable(ht, s4);
//	inserareHashTable(ht, s5);
//	
//	afisareHashTable(ht);
//}
