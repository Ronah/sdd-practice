//#include<iostream>
//
//using namespace std;
//
//struct Student {
//	int id;
//	char* nume;
//	char* prenume;
//};
//
//struct NodAVL {
//	Student* stud;
//	int ge;
//	NodAVL* st;
//	NodAVL* dr;
//};
//
//Student* creareStudent(int id, char* nume, char* prenume)
//{
//	Student* stud = new Student;
//	stud->id = id;
//	
//	stud->nume = new char[strlen(nume) + 1];
//	strcpy(stud->nume, nume);
//
//	stud->prenume = new char[strlen(prenume) + 1];
//	strcpy(stud->prenume, prenume);
//
//	return stud;
//}
//
//void afisareStudent(Student* stud)
//{
//	cout << stud->id << " " << stud->nume << " " << stud->prenume << endl;
//}
//
//int max(int a, int b)
//{
//	if (a > b)
//		return a;
//	else
//		return b;
//}
//
//int height(NodAVL* rad)
//{
//	if (rad)
//		return 1 + max(height(rad->st), height(rad->dr));
//	else
//		return 0;
//}
//
//void gradEchilibru(NodAVL* rad)
//{
//	if (rad)
//		rad->ge = height(rad->dr) - height(rad->st);
//}
//
//
//NodAVL* rotatieSimplaStanga(NodAVL* pivot, NodAVL* fiuDR)
//{
//	pivot->dr = fiuDR->st;
//	gradEchilibru(pivot);
//	fiuDR->st = pivot;
//	gradEchilibru(fiuDR);
//
//	return fiuDR;
//}
//
//NodAVL* rotatieSimplaDreapta(NodAVL* pivot, NodAVL* fiuSt)
//{
//	pivot->st = fiuSt->dr;
//	gradEchilibru(pivot);
//	fiuSt->dr = pivot;
//	gradEchilibru(fiuSt);
//
//	return fiuSt;
//}
//
//NodAVL* rotatieDublaStDr(NodAVL* pivot, NodAVL* fiuSt)
//{
//	pivot->st = rotatieSimplaStanga(fiuSt, fiuSt->dr);
//	gradEchilibru(pivot);
//	fiuSt = pivot->st;
//	fiuSt = rotatieSimplaDreapta(pivot, fiuSt);
//	gradEchilibru(fiuSt);
//
//	return fiuSt;
//}
//
//NodAVL* rotatieDublaDrSt(NodAVL* pivot, NodAVL* fiuDr)
//{
//	pivot->dr = rotatieSimplaDreapta(fiuDr, fiuDr->st);
//	gradEchilibru(pivot);
//	fiuDr = pivot->dr;
//	fiuDr = rotatieSimplaStanga(pivot, fiuDr);
//	gradEchilibru(fiuDr);
//
//	return fiuDr;
//}
//
//NodAVL* inserareAVL(NodAVL* rad, Student* stud)
//{
//	if (rad)
//	{
//		if (rad->stud->id < stud->id)
//			rad->dr = inserareAVL(rad->dr, stud);
//		else
//			if (rad->stud->id > stud->id)
//				rad->st = inserareAVL(rad->st, stud);
//			else
//				cout << "Elementul este deja prezent in arbore.";
//	}
//	else
//	{
//		NodAVL* nou = new NodAVL;
//		nou->stud = stud;
//		nou->dr = NULL;
//		nou->st = NULL;
//		
//		rad = nou;
//	}
//
//
//	gradEchilibru(rad);
//	if (rad->ge == 2)
//		if (rad->ge == -1)
//			rad = rotatieDublaDrSt(rad, rad->dr);
//		else
//			rad = rotatieSimplaStanga(rad, rad->dr);
//	else
//		if (rad->ge == -2)
//		{
//			if (rad->ge == 1)
//				rad = rotatieDublaStDr(rad, rad->st);
//			else
//				rad = rotatieSimplaDreapta(rad, rad->st);
//		}
//
//	return rad;
//}
//
//void SRD(NodAVL* rad)
//{
//	if (rad)
//	{
//		SRD(rad->st);
//		cout << rad->ge << endl;
//		afisareStudent(rad->stud);
//		SRD(rad->dr);
//	}
//}
//
//void main()
//{
//	Student* s1 = creareStudent(1, "AAAA", "aaaa");
//	Student* s2 = creareStudent(2, "BBBB", "bbbbb");
//	Student* s3 = creareStudent(3, "CCCC", "cccc");
//	Student* s4 = creareStudent(4, "DDDD", "dddd");
//	Student* s5 = creareStudent(5, "EEEE", "eeee");
//	Student* s6 = creareStudent(6, "FFFF", "ffff");
//	Student* s7 = creareStudent(7, "GGGG", "ggggg");
//	Student* s8 = creareStudent(8, "HHHH", "hhhh");
//	Student* s9 = creareStudent(9, "IIII", "iiii");
//	Student* s10 = creareStudent(10, "JJJJ", "jjjj");
//
//	NodAVL* arbore = NULL;
//	arbore = inserareAVL(arbore, s1);
//	arbore = inserareAVL(arbore, s2);	
//	arbore = inserareAVL(arbore, s3);
//	arbore = inserareAVL(arbore, s4);	
//	arbore = inserareAVL(arbore, s5);
//	arbore = inserareAVL(arbore, s6);	
//	arbore = inserareAVL(arbore, s7);
//	arbore = inserareAVL(arbore, s8);	
//	arbore = inserareAVL(arbore, s9);
//	arbore = inserareAVL(arbore, s10);
//
//	SRD(arbore);
//}