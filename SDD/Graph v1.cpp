//#include<iostream>
//
//using namespace std;
//
//struct Student {
//	char* nume;
//	char* prenume;
//	int id;
//};
//
//struct NodPrincipal;
//
//struct NodSecundar {
//	double cost;
//	NodPrincipal* principal;
//	NodSecundar* next;
//};
//
//struct NodPrincipal {
//	Student* stud;
//	NodPrincipal* next;
//	NodSecundar* vecini;
//};
//
//struct NodVizitati {
//	NodPrincipal* principal;
//	NodVizitati* next;
//};
//
//struct Stiva {
//	NodPrincipal* np;
//	Stiva* next;
//};
//
//Stiva* inserareInceput(Stiva* stiv, NodPrincipal* np){
//	Stiva* st = new Stiva;
//	st->next = stiv;
//	st->np = np;
//
//	return st;
//}
//
//Stiva* extragereInceput(Stiva* stiv, NodPrincipal*& np)
//{
//	np = stiv->np;
//	return stiv->next;
//}
//
//void inserareNodVizitat(NodVizitati*& nv, NodPrincipal* np)
//{
//	NodVizitati* temp = nv;
//	if (nv == NULL)
//	{
//		NodVizitati* nou = new NodVizitati;
//		nou->next = NULL;
//		nou->principal = np;
//
//		nv = nou;
//	}
//	else {
//		while (temp->next)
//			temp = temp->next;
//		NodVizitati* nou = new NodVizitati;
//		nou->next = NULL;
//		nou->principal = np;
//
//		temp->next = nou;
//	}
//}
//
//bool cautareNodVizitat(NodVizitati* nv, NodPrincipal* np)
//{
//	bool status = false;
//	NodVizitati* temp = nv;
//	while (temp)
//	{
//		if (temp->principal->stud->id == np->stud->id)
//			status = true;
//		temp = temp->next;
//	}
//	return status;
//}
//
//NodVizitati* parcurgereDF(NodPrincipal* np)
//{
//	NodVizitati* vizitat = NULL;
//	NodVizitati* lista = NULL;
//
//	Stiva* st = NULL;
//
//	st = inserareInceput(st, np);
//	inserareNodVizitat(vizitat, np);
//	while (st)
//	{
//		NodPrincipal* npt = NULL;
//		st = extragereInceput(st, npt);
//
//		inserareNodVizitat(lista, npt);
//
//		NodSecundar* temp = npt->vecini;
//		while (temp)
//		{
//			if (!cautareNodVizitat(vizitat, temp->principal))
//			{
//				if (temp->principal != NULL)
//				{
//					inserareNodVizitat(vizitat, temp->principal);
//					st = inserareInceput(st, temp->principal);
//				}
//			}
//			temp = temp->next;
//		}
//	}
//
//	return lista;
//}
//
//NodPrincipal* inserareInceput(NodPrincipal* list, Student* stud)
//{
//	NodPrincipal* nou = new NodPrincipal;
//	nou->stud = stud;
//	nou->next = list;
//	nou->vecini = NULL;
//	
//	return nou;
//}
//
//
//Student* creareStudent(char* nume, char* prenume, int id)
//{
//	Student* stud = new Student;
//	stud->id = id;
//
//	stud->nume = new char[strlen(nume) + 1];
//	strcpy(stud->nume, nume);
//
//	stud->prenume = new char[strlen(prenume) + 1];
//	strcpy(stud->prenume, prenume);
//
//	return stud;
//}
//
//void afisareStudent(Student* stud)
//{
//	cout << stud->id << " " << stud->nume << " " << stud->prenume << endl;
//}
//
//void afisareNodPrincipal(NodPrincipal* np)
//{
//	NodPrincipal* temp = np;
//	while (temp)
//	{
//		afisareStudent(temp->stud);
//		temp = temp->next;
//	}
//}
//
//void inserareListaAdiacenta(NodPrincipal* np, Student* stud, Student* toInsert)
//{
//	NodPrincipal* npt = np;
//	while (npt->stud->id != stud->id)
//	{
//		npt = npt->next;
//	}
//	
//	NodPrincipal* nti = np;
//	while (nti->stud->id != toInsert->id)
//	{
//		nti = nti->next;
//	}
//
//	if (npt->vecini == NULL)
//	{
//		NodSecundar* ns = new NodSecundar;
//		ns->cost = toInsert->id;
//		ns->next = NULL;
//		ns->principal = nti;
//		npt->vecini = ns;
//	}
//	else
//	{
//		NodSecundar* ns = npt->vecini;
//		while (ns->next)
//		{
//			ns = ns->next;
//		}
//		NodSecundar* toBeInserted = new NodSecundar;
//		toBeInserted->next = NULL;
//		toBeInserted->principal = nti;
//		toBeInserted->cost = toInsert->id;
//		ns->next = toBeInserted;
//	}
//}
//
//void afisareGraf(NodPrincipal* np)
//{
//	NodPrincipal* tnp = np;
//	while (tnp)
//	{
//		NodSecundar* ns = tnp->vecini;
//		afisareStudent(tnp->stud);
//		while (ns)
//		{
//			cout << ns->cost << endl;
//			ns = ns->next;
//		}
//		cout << endl << endl;
//		tnp = tnp->next;
//	}
//}
//
//void afisareNodVizitat(NodVizitati* nv)
//{
//	NodVizitati* temp = nv;
//	while (temp)
//	{
//		afisareStudent(temp->principal->stud);
//		temp = temp->next;
//	}
//}
//
//void main()
//{
//	Student* s1 = creareStudent("AAAA", "aaaa", 1);
//	Student* s2 = creareStudent("BBBB", "bbb", 2);
//	Student* s3 = creareStudent("CCC", "cccccc", 3);
//	Student* s4 = creareStudent("DDDDD", "dd", 4);
//	Student* s5 = creareStudent("EEEEE", "eeee", 5);
//	Student* s6 = creareStudent("FFFF", "ff", 6);
//
//	NodPrincipal* graf = new NodPrincipal;
//	graf->stud = s6;
//	graf->vecini = NULL;
//	graf->next = NULL;
//
//	graf = inserareInceput(graf, s5);
//	graf = inserareInceput(graf, s4);
//	graf = inserareInceput(graf, s3);
//	graf = inserareInceput(graf, s2);
//	graf = inserareInceput(graf, s1);
//
//	afisareNodPrincipal(graf);
//	inserareListaAdiacenta(graf, s1, s3);
//	inserareListaAdiacenta(graf, s1, s2);
//
//	inserareListaAdiacenta(graf, s3, s1);
//	inserareListaAdiacenta(graf, s3, s5);
//
//	inserareListaAdiacenta(graf, s2, s1);
//	inserareListaAdiacenta(graf, s2, s5);
//
//	inserareListaAdiacenta(graf, s5, s3);
//	inserareListaAdiacenta(graf, s5, s2);
//	inserareListaAdiacenta(graf, s5, s4);
//	inserareListaAdiacenta(graf, s5, s6);
//
//	inserareListaAdiacenta(graf, s4, s3);
//	inserareListaAdiacenta(graf, s4, s5);
//	inserareListaAdiacenta(graf, s4, s6);
//
//	inserareListaAdiacenta(graf, s6, s4);
//	inserareListaAdiacenta(graf, s6, s5);
//
//	afisareGraf(graf);
//
//	cout << endl << endl << endl << endl;
//	NodVizitati* df = parcurgereDF(graf);
//
//	afisareNodVizitat(df);
//}